/*
* required jQuery
*/
;(function($){
	/* jQuery plugin template */
	var plugname = "flickable";
	$.fn[plugname] = function(method) {
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' + method + ' does not exist on jQuery.' + plugname );
			return this;
		}
	};

	var methods = {}

	/* override */
	methods.init = function(config) {
		return this.each(function(i) {
			var defaults = {
				"flickable" : true,
				"margin" : 20,
				"duration" : 200,
				"interval" : 4000,
				"pagination" : true,
				"timerid" : 0,
				"pages" : $(this).children().length
			};
			var options = $.extend(defaults, config);
			var wrapper = $('<div />').addClass(plugname + "-wrapper");
			$(this).wrap(wrapper);
			$(this).addClass('inner');
			$(this).children().addClass('item');
			$(this).data("options", options);
			var start={x:0,y:0}, move={x:0,y:0};
			$(this).data("start", start);
			$(this).data("move", move);
			
			$(this)[plugname]("auto");
			if(options.flickable) {
				$(this)[plugname]("flickable");
			}
			if(options.pagination) {
				$(this)[plugname]("pagination");
			}
		});
	}
	
	/*
	 * フリック設定
	 */
	methods.flickable = function() {
		$(this).bind('touchstart', {}, touchHandler);
		$(this).bind('touchmove', {}, touchHandler);
		$(this).bind('touchend', {}, touchHandler);
		return this;
	}
	
	/*
	* 左スライド
	*/
	methods.left = function(duration) {
		var options = $(this).data('options');
		var width = $(this).width();
		var $pager = $(this).next(".flickable-pager");
		duration = duration ? duration : options.duration;
		
		var x = Math.abs($(this).position().left);
		var page = Math.floor(x / width);
		var next = ((page + 1) < options.pages) ? page + 1 : 0;
		$(this).animate({ "left" : -(width * next) }, duration, "linear", function() {
			if($pager) {
				var page = next;
				$pager.find('.page.active').removeClass('active');
				$pager.find('.page:eq(' + page + ')').addClass('active');
				if($pager.find('.page:last').hasClass('active')) {
					$pager.find('.next a').addClass('disabled');
					$pager.find('.prev a').removeClass('disabled');
				} else if($pager.find('.page:first').hasClass('active')) {
					$pager.find('.prev a').addClass('disabled');
					$pager.find('.next a').removeClass('disabled');
				} else {
					$pager.find('.next a').removeClass('disabled');
					$pager.find('.prev a').removeClass('disabled');
				}
			}
		});
		return this;
	}
	
	/*
	* 右スライド
	*/
	methods.right = function(duration) {
		var options = $(this).data('options');
		var width = $(this).width();
		var $pager = $(this).next(".flickable-pager");
		duration = duration ? duration : options.duration;
		
		var x = Math.abs($(this).position().left);
		var page = Math.floor(x / width);
		var next = ((page - 1) < 0) ? 0 : (page - 1);
		$(this).animate({ "left" : -(width * next) }, duration, "linear", function() {
			if($pager) {
				var page = next;
				$pager.find('.page.active').removeClass('active');
				$pager.find('.page:eq(' + page + ')').addClass('active');
				if($pager.find('.page:first').hasClass('active')) {
					$pager.find('.next a').removeClass('disabled');
					$pager.find('.prev a').addClass('disabled');
				} else if($pager.find('.page:last').hasClass('active')) {
					$pager.find('.prev a').removeClass('disabled');
					$pager.find('.next a').addClass('disabled');
				} else {
					$pager.find('.next a').removeClass('disabled');
					$pager.find('.prev a').removeClass('disabled');
				}
			}
		});
		return this;
	}
	
	/*
	* 自動スライド開始
	*/
	methods.auto = function(duration, interval) {
		var obj = this;
		var options = $(this).data('options');
		duration = duration ? duration : options.duration;
		interval = interval ? interval : options.interval;
		if(interval == 0) { return this; }
		
		var timerid = setInterval(function() {
			$(obj)[plugname]("left", duration);
		}, interval);
		options.timerid = timerid;
		$(this).data('options', options);
		return this;
	}
	
	/*
	* 自動スライド停止
	*/
	methods.stop = function() {
		var options = $(this).data('options');
		var timerid = options.timerid;
		if(timerid) {
			clearInterval(timerid);
			options.timerid = 0;
			$(this).data('options', options);
		}
		return this;
	}
	/*
	* ページャー
	*/
	methods.pagination = function(duration) {
		var options = $(this).data('options');
		var $ul = $('<ul />').addClass('flickable-pager').clone();
		var $prev = $('<a href="#prev" />').addClass("btn");
		var $next = $('<a href="#next" />').addClass("btn");
		$prev.html($('<span />').html('Prev'));
		$next.html($('<span />').html('Next'));
		
		var target = this;
		$prev.click(function(e) {
			var left = Math.abs($(target).position().left);
			var page = parseInt(left / $(target).width());
			if(0 < page) {
				$(target)[plugname]("stop");
				$(target)[plugname]('right', duration);
				$(target)[plugname]("auto");
			}
			return false;
		}).addClass("disabled");
		
		$next.click(function(e) {
			var left = Math.abs($(target).position().left);
			var page = parseInt(left / $(target).width());
			if(page < (options.pages - 1)) {
				$(target)[plugname]("stop");
				$(target)[plugname]('left', duration);
				$(target)[plugname]("auto");
			}
			return false;
		});
		$ul.append($('<li />').html($prev).addClass("prev"));
		for(var i = 0; i < $(this).children().length; i++) {
			$ul.append($('<li />').addClass('page'));
		}
		$ul.append($('<li />').html($next).addClass("next"));
		
		$ul.find('.page:first').addClass('active');
		$(this).after($ul);
		options.pager = $ul;
		$(this).data("options", options);
		return this;
	}
	
	/* private method */
	var touchHandler = function(e) {
		var event = e.originalEvent.touches ? e.originalEvent.touches[0] : e,
		start = $(this).data("start"), move = $(this).data("move");
		var options = $(this).data("options");
		
		if(e.type == 'touchstart') {
			$(this)[plugname]("stop");
			start.x = $(this).position().left,
			start.y = $(this).position().top,
			move.x = event.pageX,
			move.y = event.pageY;
		} else if(e.type == 'touchmove') {
			var diff = {
			"x" : event.pageX - move.x,
			"y" : event.pageY - move.y
			};
			move.x = event.pageX;
			move.y = event.pageY;
			
			if(Math.abs(diff.x) - Math.abs(diff.y) > 0) {
				e.preventDefault();
				$(this).css({"left" : $(this).position().left + diff.x});
			}
		} else if(e.type == 'touchend') {
			var left = $(this).position().left;
			var diff = left - start.x;
			
			var reflection = (Math.abs(diff) < options.margin);
			reflection |= (left < -($(this).width() * (options.pages - 1)));
			reflection |= (0 < left);
			
			if(reflection) {
				$(this)[plugname]("auto")
				$(this).animate({"left" : start.x}, 100, "linear");
			} else if(0 < diff) {
				$(this)[plugname]("right");
			} else if(diff < 0) {
				$(this)[plugname]("left");
			}
		}
		
		$(this).data("start", start);
		$(this).data("move", move);
	}

})(jQuery);